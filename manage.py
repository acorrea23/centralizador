import redis
from rq import Connection, Worker
from flask.cli import FlaskGroup

from nuaio_scraper import create_app


app = create_app()
cli = FlaskGroup(create_app=create_app)


@cli.command('run_worker')
def run_worker():
    redis_connection = redis.from_url(app.config['REDIS_URL'])
    with Connection(redis_connection):
        worker = Worker(app.config['REDIS_QUEUE'])
        worker.work()


if __name__ == "__main__":
    cli()