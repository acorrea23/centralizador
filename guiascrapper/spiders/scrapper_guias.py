import scrapy
import json


class EnviaSpider(scrapy.Spider):

    name = 'envia'

    def __init__(self, guia):
        self.guia = guia
        self.start_urls = [f'https://portal.envia.co/ServicioRestConsultaEstados/Service1Consulta.svc/ConsultaGuia/{self.guia}']

    def parse(self, response, **kwargs):
        r_bytes = response.body
        r_json = json.loads(r_bytes.decode('utf-8'))

        estados = []
        if r_json['fecha_produccion']:
            estados.append({
                'statusCode': '01',
                'statusDate': r_json['fecha_produccion'],
                'statusInfo': 'Pronto será recogido el pedido',
                'statusName': 'Orden creada'
            })
        if r_json['fec_recoleccion']:
            estados.append({
                'statusCode': '02',
                'statusDate': r_json['fec_recoleccion'],
                'statusInfo': None,
                'statusName': 'Pedido recogido'
            })
        if r_json['fec_despacho']:
            estados.append({
                'statusCode': '03',
                'statusDate': r_json['fec_despacho'],
                'statusInfo': 'El pedido va en camino a la ciudad destino',
                'statusName': 'En transporte'
            })
        if r_json['fec_bodegadestino']:
            estados.append({
                'statusCode': '04',
                'statusDate': r_json['fec_bodegadestino'],
                'statusInfo': 'El pedido se encuentra en la ciudad destino',
                'statusName': 'En bodega'
            })
        if r_json['fec_reparto']:
            estados.append({
                'statusCode': '05',
                'statusDate': r_json['fec_bodegadestino'],
                'statusInfo': 'El pedido va en camino a su dirección destino',
                'statusName': 'En distribución'
            })
        if r_json['fecha_entrega']:
            estados.append({
                'statusCode': '06',
                'statusDate': r_json['fecha_entrega'] + ' ' + r_json['hora'],
                'statusInfo': None,
                'statusName': 'Entregado'
            })

        #sorted_estados = sorted(estados, key=lambda x: datetime.strptime(x['statusDate'], '%m/%d/%Y %H:%M'))

        result = {
            'trackingNumber': r_json['guia'],
            'shipFrom': r_json['ciudad_origen'],
            'shipTo': r_json['ciudad_destino'],
            'imageURL': r_json['imagen'],
            'statusList': estados
        }

        yield result


class CoordinadoraSpider(scrapy.Spider):
    name = 'coordinadora'

    search_url = 'https://www.coordinadora.com/portafolio-de-servicios/servicios-en-linea/rastrear-guias/'
    start_urls = [search_url]

    def __init__(self, guia):
        self.guia = guia

    def parse(self, response, **kwargs):

        if self.guia:
            data = {'coor_guia': self.guia,
                    'coor_guia_home': 'true'}

            if self.start_urls:
                yield scrapy.FormRequest(url=self.search_url, formdata=data, callback=self.parse_guia, method='POST')

    def parse_guia(self, response):
        guia = response.css('div.numero-guia::text').get().strip()
        info_destino = response.css('td.guia-val::text').getall()
        estados = response.css('div.left.desc-estado>div::text').getall()
        date_numbers = response.css('div.dateNumbers::text').getall()
        date_months = response.css('span.dateCharsMonth::text').getall()
        date_years = response.css('span.dateCharsYear::text').getall()
        date_time = response.css('div.dateCharsTime::text').getall()

        statusList = []

        for i in range(len(estados)):
            if estados[i] == 'A RECIBIR POR COORDINADORA':
                statusList.append({
                    'statusCode': '01',
                    'statusDate': f'{date_numbers[i]}/{date_months[i]}/{date_years[i].strip()}',
                    'statusInfo': None,
                    'statusName': 'Orden creada'
                })
            elif estados[i] == 'EN TERMINAL ORIGEN':
                statusList.append({
                    'statusCode': '01',
                    'statusDate': f'{date_numbers[i]}/{date_months[i]}/{date_years[i].strip()}',
                    'statusInfo': None,
                    'statusName': 'Pedido recogido'
                })
            elif estados[i] == 'EN TRANSPORTE':
                statusList.append({
                    'statusCode': '03',
                    'statusDate': f'{date_numbers[i]}/{date_months[i]}/{date_years[i].strip()}',
                    'statusInfo': 'El pedido va en camino a la ciudad destino',
                    'statusName': 'En transporte'
                })
            elif estados[i] == 'EN TERMINAL DESTINO':
                statusList.append({
                    'statusCode': '04',
                    'statusDate': f'{date_numbers[i]}/{date_months[i]}/{date_years[i].strip()}',
                    'statusInfo': 'El pedido se encuentra en la ciudad destino',
                    'statusName': 'En bodega'
                })
            elif estados[i] == 'ENTREGADA':
                statusList.append({
                    'statusCode': '06',
                    'statusDate': f'{date_numbers[i]}/{date_months[i]}/{date_years[i].strip()}',
                    'statusInfo': None,
                    'statusName': 'Entregado'
                })
            else:
                statusList.append({
                    'statusCode': '07',
                    'statusDate': f'{date_numbers[i]}/{date_months[i]}/{date_years[i].strip()}',
                    'statusInfo': None,
                    'statusName': estados[i]
                })

        # sorted_statusList = sorted(statusList, key=lambda x: datetime.strptime(x['statusDate'], '%m/%d/%Y'))

        result = {
            'trackingNumber': guia,
            'shipFrom': info_destino[0],
            'shipTo': info_destino[1],
            'imageURL': None,
            'statusList': statusList
        }

        yield result
