from flask import Blueprint

bp = Blueprint('errors', __name__)

from nuaio_scraper.errors import handlers