from flask import Flask
from redis import Redis
import rq


def create_app(config_file='settings.py'):
    app = Flask(__name__)
    app.config.from_pyfile(config_file)

    # Init extensions
    from nuaio_scraper.extensions import db
    db.init_app(app)

    app.redis = Redis.from_url(app.config['REDIS_URL'])
    app.rq_queue = rq.Queue(app.config['REDIS_QUEUE'], connection=app.redis)

    # Blueprints
    from nuaio_scraper.main import bp as main_bp
    app.register_blueprint(main_bp)

    from nuaio_scraper.errors import bp as errors_bp
    app.register_blueprint(errors_bp)

    return app
