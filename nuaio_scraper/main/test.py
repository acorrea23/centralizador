import pandas as pd
import numpy as np
import requests
from itertools import chain


def georreferenciar(address, city):
    url = "http://mdepyd07:9090/geolocation/api/georeference-address"
    headers = {
        'api-key': 'dc06617c-c21e-4792-91b8-0d9ef13369dd',
        'system-code': 'FAB-APP',
        'Content-Type': 'application/json'
    }
    payload = f'{{"address": "{address}","city": "{city}"}}'
    response = requests.request("POST", url, headers=headers, data=payload)
    try:
        geo_address = response.json()['geoRecord']['address']
    except:
        geo_address = ''
    return geo_address


def geo_to_df(row):
    row['nueva_dir'] = georreferenciar(row['shipping_address_1'], row['shipping_city'])
    return row


old_orders = pd.read_excel(
    'C:\\Users\\miguel.velasquez\\OneDrive - CADENA SA\\CadenaLabs\\eLogistics\\Order Ingest\\Cruces\\CONSOLIDADO PEDIDOS.xlsx')
df = pd.read_excel(
    'C:\\Users\\miguel.velasquez\\OneDrive - CADENA SA\\CadenaLabs\\eLogistics\\Order Ingest\\REPORTE ST CADENA 29 OCTUBRE.xlsx', converters={'billing_phone': str})
codigo_depts = pd.read_excel(
    'C:\\Users\\miguel.velasquez\\OneDrive - CADENA SA\\CadenaLabs\\eLogistics\\Order Ingest\\Cruces\\Cod ISO Dept.xlsx')
codigo_divipola = pd.read_excel(
    'C:\\Users\\miguel.velasquez\\OneDrive - CADENA SA\\CadenaLabs\\eLogistics\\Order Ingest\\Cruces\\DIVIPOLA_20150630.xlsx',header=4)
hist_column_names = ['Cedula', 'Nombre', 'Departamento', 'Direccion']
historico_direcciones = pd.read_excel('C:\\Users\\miguel.velasquez\\OneDrive - CADENA '
                                      'SA\\CadenaLabs\\eLogistics\\Order Ingest\\Cruces\\Direcciones SAVVY.xlsx',
                                      header=None, names=hist_column_names)

obligatory_columns = ['Convencion',
                      'Order ID',
                      'Columna1',
                      'Status',
                      'order_date',
                      'shipping_first_name',
                      'shipping_last_name',
                      'Cedula',
                      'shipping_address_1',
                      'billing_phone',
                      'shipping_city',
                      'shipping_state',
                      'shipping_country',
                      'item_product_id',
                      'item_name',
                      'item_quantity',
                      'order_total',
                      'shipping_total']

savvy_catalog = {
    16196: ['T8.00.002402'],
    16186: ['T8.00.002401'],
    10012: ['T8.00.002397'],
    7962: ['T8.02.000547'],
    7954: ['T8.02.000548'],
    7661: ['T8.02.000546', 'T8.02.000545', 'T8.00.002396'],
    5584: ['T8.02.000546', 'T8.02.000545', 'T8.00.002398'],
    209: ['T8.02.000546'],
    200: ['T8.02.000545'],
    5939: ['T8.00.002396'],
    5280: ['T8.00.002398'],
    17990: ['T8.02.000546', 'T8.02.000545', 'T8.00.002397'],
    17994: ['T8.00.002402', 'T8.00.002401', 'T8.00.002398'],
    17980: ['T8.02.000547', 'T8.02.000548', 'T8.00.002396'],
    19770: ['T8.02.000547', 'T8.02.000545'],
    19794: ['T8.02.000548', 'T8.02.000545'],
    19789: ['T8.02.000547', 'T8.02.000546'],
    19796: ['T8.02.000548', 'T8.02.000546'],
    20129: ['T8.02.000555', 'T8.02.000553', 'T8.00.002412'],
    20125: ['T8.02.000554', 'T8.02.000554',
            'T8.02.000556', 'T8.02.000556',
            'T8.02.000557', 'T8.02.000557',
            'T8.02.000558', 'T8.02.000558',
            'T8.00.002407'],
    21332: ['T8.02.000554', 'T8.02.000554',
            'T8.02.000556', 'T8.02.000556',
            'T8.02.000557', 'T8.02.000557',
            'T8.02.000558', 'T8.02.000558',
            'T8.00.002409'],
    21336: ['T8.02.000554', 'T8.02.000554',
            'T8.02.000556', 'T8.02.000556',
            'T8.02.000557', 'T8.02.000557',
            'T8.02.000558', 'T8.02.000558',
            'T8.00.002408'],
    23682: ['T8.00.002414'],
    23681: ['T8.00.002415'],
    23680: ['T8.00.002416'],
    23684: ['T8.00.002417'],
    23686: ['T8.00.002418'],
    23687: ['T8.00.002419'],
    23689: ['T8.00.002420'],
    23670: ['T8.00.002421'],
    23671: ['T8.00.002422'],
    23672: ['T8.00.002423'],
    23674: ['T8.00.002424'],
    23676: ['T8.00.002425'],
    23677: ['T8.00.002426'],
    23678: ['T8.00.002427'],
    23662: ['T8.00.002428'],
    23664: ['T8.00.002429'],
    23665: ['T8.00.002430'],
    23666: ['T8.00.002431'],
    24193: ['T8.00.002432'],
    23658: ['T8.00.002433'],
    23657: ['T8.00.002434'],
    23660: ['T8.00.002435'],
    26916: ['T8.02.000555', 'T8.02.000553', 'T8.00.002412', 'T8.00.002398', 'T8.00.002447']
}

empty_info_df = df[df[obligatory_columns].isna().any(1)]

info_df = df[df[obligatory_columns].notna().all(1)]
# Check numeric Order ID
info_df = info_df[pd.to_numeric(info_df['Order ID'], errors='coerce').notnull()]
# CONV == 'P'
info_df = info_df[info_df['Convencion'] == 'P']

# Sacar todos los pedidos que no quedaron en info_df
error_df = df[~df[df.isin(info_df)].any(1)]
error_df = df[df.merge(info_df.drop_duplicates(),
                       on=obligatory_columns, how='left', indicator=True)['_merge'] == 'left_only']
# Cruzar para sacar ordenes que no existieran
new_orders = info_df[info_df.merge(old_orders.drop_duplicates(),
                                   left_on='Order ID', right_on='PEDIDO', how='left',
                                   indicator=True)['_merge'] == 'left_only']

# Obtener direcciones unicas (1 direccion / orden)
base_direcciones = info_df.drop_duplicates(subset='Order ID', keep="first").reset_index(drop=True)

# Limpiar direcciones: quedan en columna nueva_dir. Puede quedar vacio
# base_direcciones= base_direcciones.apply(geo_to_df, axis=1)
# base_direcciones['Clean Address'] = base_direcciones['nueva_dir'].where(base_direcciones['nueva_dir'] != '', base_direcciones['shipping_address_1'])


def limpiar_direcciones(df, old_column, new_column):
    df[new_column] = df[old_column]
    df[new_column] = df[new_column].str.upper()
    df[new_column] = df[new_column].str.replace('#', '')
    df[new_column] = df[new_column].str.replace('.', '')
    df[new_column] = df[new_column].str.replace(',', '')
    df[new_column] = df[new_column].str.replace('/', '')
    df[new_column] = df[new_column].str.replace('  ', ' ')
    df[new_column] = df[new_column].str.replace('CALLE', 'CL')
    df[new_column] = df[new_column].str.replace('CLLE', 'CL')
    df[new_column] = df[new_column].str.replace('OFICINA', 'OF')
    df[new_column] = df[new_column].str.replace('APARTAMENTO', 'AP')
    df[new_column] = df[new_column].str.replace('BARRIO', 'BR')
    df[new_column] = df[new_column].str.replace('TORRE', 'TR')
    df[new_column] = df[new_column].str.replace('CONJUNTO', 'CJ')
    df[new_column] = df[new_column].str.replace('CASA', 'CS')
    df[new_column] = df[new_column].str.replace('CARRERA', 'CR')
    df[new_column] = df[new_column].str.replace('UNIDAD', 'UN')
    df[new_column] = df[new_column].str.replace('AVENIDA', 'AV')
    df[new_column] = df[new_column].str.replace('URBANIZACION', 'URB')
    df[new_column] = df[new_column].str.replace('URBANIZACIÓN', 'URB')
    df[new_column] = df[new_column].str.replace('EDIFICIO', 'ED')
    df[new_column] = df[new_column].str.replace('CONDOMINIO', 'CD')
    df[new_column] = df[new_column].str.replace('TRANSVERSAL', 'TV')
    df[new_column] = df[new_column].str.replace('MANZANA', 'MZ')
    df[new_column] = df[new_column].str.replace('CIRCUNVALAR', 'CRV')
    df[new_column] = df[new_column].str.replace('CIRCUMVALAR', 'CRV')
    df[new_column] = df[new_column].str.replace('CIRCUMBALAR', 'CRV')
    df[new_column] = df[new_column].str.replace('CIRCUMVALAR', 'CRV')
    df[new_column] = df[new_column].str.replace('DIAGONAL', 'DG')
    df[new_column] = df[new_column].str.replace('AGRUPACION', 'AGP')
    df[new_column] = df[new_column].str.replace('AGRUPACIÓN', 'AGP')
    df[new_column] = df[new_column].str.replace('ALMACÉN', 'ALM')
    df[new_column] = df[new_column].str.replace('ALMACEN', 'ALM')
    df[new_column] = df[new_column].str.replace('AUTOPISTA', 'AUT')
    df[new_column] = df[new_column].str.replace('BODEGA', 'BG')
    df[new_column] = df[new_column].str.replace('BLOQUE', 'BL')
    df[new_column] = df[new_column].str.replace('CORREGIMIENTO', 'C')
    df[new_column] = df[new_column].str.replace('CENTRO COMERCIAL', 'CC')
    df[new_column] = df[new_column].str.replace('CIRCULAR', 'CIR')
    df[new_column] = df[new_column].str.replace('CONJUNTO', 'CONJ')
    df[new_column] = df[new_column].str.replace('DEPARTAMENTO', 'DPTO')
    df[new_column] = df[new_column].str.replace('ESQUINA', 'ESQ')
    df[new_column] = df[new_column].str.replace('GLORIETA', 'GT')
    df[new_column] = df[new_column].str.replace('KILOMETRO', 'KM')
    df[new_column] = df[new_column].str.replace('KILÓMETRO', 'KM')
    df[new_column] = df[new_column].str.replace('OCCIDENTE', 'OCC')
    df[new_column] = df[new_column].str.replace('ENTRADA', 'EN')
    df[new_column] = df[new_column].str.replace('FINCA', 'FCA')


limpiar_direcciones(base_direcciones, 'shipping_address_1', 'Clean Address')
limpiar_direcciones(historico_direcciones, 'Direccion', 'Clean Address')

merged_addresses = base_direcciones.merge(historico_direcciones.drop_duplicates(),
                                                        left_on='Cedula',
                                                        right_on='Cedula',
                                                        how='left',
                                                        indicator=True)

new_addresses = base_direcciones[merged_addresses['_merge'] == 'left_only']

# TODO: cycle to check historic addresses
done = False
i = 1
while not done:

    matched_addresses = base_direcciones[base_direcciones.merge(historico_direcciones.drop_duplicates(),
                                                        left_on='Cedula',
                                                        right_on='Cedula',
                                                        how='left',
                                                        indicator=True)['_merge'] == 'both']

    direcciones_diferentes = matched_addresses[matched_addresses.merge(historico_direcciones.drop_duplicates(),
                                                        left_on='Clean Address',
                                                        right_on='Clean Address',
                                                        how='left',
                                                        indicator=True).set_index(matched_addresses.index)['_merge'] == 'left_only']

    if len(direcciones_diferentes) != 0:
        direcciones_diferentes['new_id'] = direcciones_diferentes['Cedula'].astype(str) + '-1'
        new_addresses.append(direcciones_diferentes)

new_addresses = new_addresses.merge(codigo_depts,
                                    left_on='shipping_state',
                                    right_on='SIGLA',
                                    how='left')
new_addresses['shipping_city'] = new_addresses['shipping_city'].str.replace('á', 'a')
new_addresses['shipping_city'] = new_addresses['shipping_city'].str.replace('é', 'e')
new_addresses['shipping_city'] = new_addresses['shipping_city'].str.replace('í', 'i')
new_addresses['shipping_city'] = new_addresses['shipping_city'].str.replace('ó', 'o')
new_addresses['shipping_city'] = new_addresses['shipping_city'].str.replace('ú', 'u')
new_addresses['shipping_city'] = new_addresses['shipping_city'].str.replace('Bog ota D.C', 'Bogota')
new_addresses['llaveDivipola'] = new_addresses['DEPARTAMENTO'] + new_addresses['shipping_city'] + new_addresses['shipping_city']

new_addresses['BusinessPartnerNo'] = pd.Series(np.repeat(900285758, len(info_df)))
new_addresses['CompanyName'] = pd.Series(np.repeat('C.I. NUTREO S.A.S.', len(info_df)))
new_addresses['City'] = new_addresses.merge(codigo_divipola,
                                            left_on='llaveDivipola',
                                            right_on='Llave',
                                            how='left')['Código Centro Poblado']
new_addresses['PostalCode'] = pd.Series(np.repeat('', len(info_df)))
new_addresses['Code'] = new_addresses['Cedula']
new_addresses['AliasName'] = (new_addresses['shipping_first_name'] + ' ' + new_addresses['shipping_last_name'])
new_addresses['FirstName'] = (new_addresses['shipping_first_name'] + ' ' + new_addresses['shipping_last_name'])
new_addresses['CountryCode'] = new_addresses['shipping_country']
new_addresses['State'] = new_addresses['shipping_state']
new_addresses['Address'] = new_addresses['Clean Address']
new_addresses['PrimaryRoadType'] = pd.Series(np.repeat('', len(info_df)))
new_addresses['PrimaryNumber'] = pd.Series(np.repeat('', len(info_df)))
new_addresses['PrimaryIdentifier'] = pd.Series(np.repeat('', len(info_df)))
new_addresses['PrimaryQuadrant'] = pd.Series(np.repeat('', len(info_df)))
new_addresses['SecondaryNumber'] = pd.Series(np.repeat('', len(info_df)))
new_addresses['SecondaryIdentifier'] = pd.Series(np.repeat('', len(info_df)))
new_addresses['SecondaryQuadrant'] = pd.Series(np.repeat('', len(info_df)))
new_addresses['PlateNumber'] = pd.Series(np.repeat('', len(info_df)))
new_addresses['AdditionalInfo'] = pd.Series(np.repeat('', len(info_df)))
new_addresses['Phone'] = new_addresses['billing_phone'].str.replace('\W', '').str[-10:]

direcciones_columns = ['BusinessPartnerNo',
                       'CompanyName',
                       'City',
                       'PostalCode',
                       'Code',
                       'AliasName',
                       'FirstName',
                       'CountryCode',
                       'State',
                       'Address',
                       'PrimaryRoadType',
                       'PrimaryNumber',
                       'PrimaryIdentifier',
                       'PrimaryQuadrant',
                       'SecondaryNumber',
                       'SecondaryIdentifier',
                       'SecondaryQuadrant',
                       'PlateNumber',
                       'AdditionalInfo',
                       'Phone']
savvy_df = new_orders['item_product_id'].map(savvy_catalog).dropna()
temp_product_df = new_orders.loc[savvy_df.index].reset_index(drop=True).merge(codigo_depts,
                                                                              left_on='shipping_state',
                                                                              right_on='SIGLA',
                                                                              how='left',
                                                                              indicator=True)
len_s = savvy_df.str.len()
new_product_df = pd.DataFrame({
    'No Compra': temp_product_df['Order ID'].values.repeat(len_s),
    'SKU(producto)': list(chain.from_iterable(savvy_df)),
    'Cantidad': temp_product_df['item_quantity'].values.repeat(len_s),
    '# Centro Costos Facturacion': temp_product_df['Cedula'].values.repeat(len_s),
    '#Centro Costos Envio': temp_product_df['Cedula'].values.repeat(len_s),
    '# Direccion Envio': temp_product_df['Cedula'].values.repeat(len_s),
    'UNO': temp_product_df['ORIGEN'].values.repeat(len_s),
    'DOS': '',
    'TRES': ''
})

pedidos_columns = ['No Compra',
                   'SKU(producto)',
                   'Cantidad',
                   '# Centro Costos Facturacion',
                   '#Centro Costos Envio',
                   '# Direccion Envio',
                   'UNO',
                   'DOS',
                   'TRES']

ceco_df = pd.DataFrame({
    'Cod. CeCo': new_addresses['Code'],
    'Nombre': new_addresses['AliasName'],
    'Presupuesto': pd.Series(np.repeat(1000000000, len(info_df))),
    'Periodo': pd.Series(np.repeat('monthly', len(info_df))),
    'ID usuario aprobador': pd.Series(np.repeat('insumosmedicossura@bazaar.com.co', len(info_df))),
    'Correo Usuarios comprador': pd.Series(np.repeat('insumosmedicossura@bazaar.com.co|sgomezr@sura.com.co|agomezh@sura.com.co', len(info_df))),
    'Todas las Direcciones': pd.Series(np.repeat(0, len(info_df))),
    'ID Direcciones': new_addresses['Code'],
    'NIT': '',
    'Nombre Filial': '',
    'Inicio Calendario': '',
    'Final Calendario': ''
})

new_addresses.to_csv('D:\\miguel.velasquez\\Desktop\\direcciones.csv', columns=direcciones_columns, sep=';')
new_product_df.to_csv('D:\\miguel.velasquez\\Desktop\\pedidos.csv', columns=pedidos_columns, sep=';')
ceco_df.to_csv('D:\\miguel.velasquez\\Desktop\\cecos.csv', sep=';')
